## ONLINE CALENDAR 

This will be a pretty & useful online calendar. There'll also be a Telegram bot to edit your profile and get notifications.
*Well, not now, but someday...*

Documentation links:
https://docs.google.com/document/d/1Lxd2kySmi9tx3j3dyJArSDmFUDd3kot_cTm8960106k/edit

Deployed Heroku version:
https://sparkle-calendar.herokuapp.com/

This two test accounts can be used to quickly see functionality:  
**simple user**:  
*login*: user  
*password*: 1234  
  
**admin**:  
*login*: sparkle\_admin  
*password*:  sparkle\_admin  
  
However, you\`re always welcome to register yourself)  
  
Admin is not very different from other users exept when searching everybody\`s events are visible for him 
(simple user can only look throuh his/her own ones).  
  
Mongo DB is being user for this project.  
  
Here are some links for it:  
  
To connect using the mongo shell: mongo ds159845.mlab.com:59845/sparkle -u <dbuser> -p <dbpassword>  
To connect using a driver via the standard MongoDB URI (what's this?): mongodb://<dbuser>:<dbpassword>@ds159845.mlab.com:59845/sparkle  
  
Owner\`s login/password for it are *admin/admin*.  

**Important**  
Don`t use telegram username to log in, as your device may suggest, use username instead. Otherwise you`ll be getting enternal server error. 
